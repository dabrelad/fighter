﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrunksController : MonoBehaviour
{
    //variables
    public int HP = 100;
    public float yForceHitted = 0.2f;
    public float xForceHitted = 0.5f;
    public bool hit = true;
    public bool bloqueo;
    public float movementF = 0.02f;
    private int comboStatus = 0;
    private bool lookingLeft = false;
    public GameObject gameController;
    public GameObject buuController;
    private Animator anim;
    Collider2D auxOther;
    private bool isGrounded = true;
    public Vector2 jumpForce = new Vector2(0f, 1f);
    float cooldownProyectil = 5f;
    Boolean cooldownProyectilB = false;
    public GameObject proyectil;

    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        #region Movement
        //Movement
        if (Input.GetKey(KeyCode.G))
        {
            this.transform.position = new Vector2(this.transform.position.x - movementF, this.transform.position.y);
            if (!lookingLeft)
            {
                this.transform.Rotate(new Vector3(0, 180, 0));
                lookingLeft = true;
            }
            anim.SetBool("finishRunningTrunks", false);
            anim.SetTrigger("runTrunks");
            
        }
        else if (Input.GetKey(KeyCode.J))
        {
            this.transform.position = new Vector2(this.transform.position.x + movementF, this.transform.position.y);
            if (lookingLeft)
            {
                this.transform.Rotate(new Vector3(0, 180, 0));
                lookingLeft = false;
            }
            anim.SetBool("finishRunningTrunks", false);
            anim.SetTrigger("runTrunks");
        }

        else if (!Input.GetKey(KeyCode.G) && !Input.GetKey(KeyCode.K))
        {
            anim.SetBool("finishRunningTrunks", true);
        }
        #endregion

        #region Attack
        //Attack

        if (Input.GetKey(KeyCode.I))
        {
            StartCoroutine(attackCombo("i"));
        }
        else if (Input.GetKey(KeyCode.O))
        {
            StartCoroutine(attackCombo("o"));
        }

        IEnumerator attackCombo(string s)
        {
            if (s == "i")
            {
                if (comboStatus == 0)
                {
                    anim.SetTrigger("attack1Trunks");
                    comboStatus = 1;
                    yield return new WaitForSeconds(0.5f);
                    if (comboStatus == 1)
                    {
                        print("combo break");
                        comboStatus = 0;
                    }
                }
            }
            if (s == "o")
            {
                if (comboStatus == 1)
                {
                    comboStatus = 0;
                    print("combo acabado");
                    anim.SetTrigger("attack2Trunks");
                }
            }

        }

        #endregion

        #region Proyectil
        if (Input.GetKeyDown(KeyCode.L))
        {
            lanzarProyectil();
        }

        void lanzarProyectil()
        {
            if (cooldownProyectilB == false)
            {
                GameObject aux;
                if (this.lookingLeft == true)
                {
                    aux = Instantiate(proyectil, new Vector3(this.GetComponent<Transform>().position.x - 1, this.GetComponent<Transform>().position.y + 1, 0), transform.rotation * Quaternion.Euler(0, 180, 0));
                    aux.GetComponent<Rigidbody2D>().AddForce(new Vector2(-100, 40));
                    cooldownProyectilB = true;
                    StartCoroutine("cdBomba");
                }
                else
                {
                    aux = Instantiate(proyectil, new Vector3(this.GetComponent<Transform>().position.x + 1, this.GetComponent<Transform>().position.y + 1, 0), Quaternion.identity);
                    aux.GetComponent<Rigidbody2D>().AddForce(new Vector2(100, 40));
                    cooldownProyectilB = true;
                    StartCoroutine("cdBomba");
                }
            }
        }

        #endregion

        #region Jump
        if (Input.GetKeyDown(KeyCode.Y) && isGrounded)
        {
            anim.SetTrigger("jumpTrunks");
            this.GetComponent<Rigidbody2D>().AddForce(jumpForce, ForceMode2D.Impulse);
        }
        #endregion

        #region shield
        //Shield
        if (Input.GetKey("k"))
        {
            bloqueo = true;
        }
        else
        {
            bloqueo = false;
        }

        if (!auxOther && hit == false)
        {
            hit = true;
        }
        #endregion

    }

    //corroutine cooldown bomba 
    IEnumerator cdBomba()
    {
        yield return new WaitForSeconds(cooldownProyectil);
        cooldownProyectilB = false;
    }

    //controlador de vida funcional x delegados desde GameController
    private void actualizarVidaTrunks()
    {
        HP -= 20;
        gameController.GetComponent<GameController>().actualizarHP();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Hitbox_Buu" && hit == true && bloqueo == false)
        {
            hit = false;
            actualizarVidaTrunks();
            golpeadoAnim();
        }
        if(other.gameObject.tag == "Hitbox_Proyectil" && hit == true)
        {
            hit = false;
            actualizarVidaTrunks();
            golpeadoAnim();
            auxOther = other;
        }
    }
   
    //fisicas al ser golpeado
    private void golpeadoAnim()
    {
        if(buuController.transform.position.x > this.transform.position.x)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-xForceHitted, yForceHitted), ForceMode2D.Impulse);
        }
        else if(buuController.transform.position.x < this.transform.position.x)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(xForceHitted, yForceHitted), ForceMode2D.Impulse);
        }
        
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Hitbox_Buu" && hit == false)
        {
            hit = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "background")
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "background")
        {
            isGrounded = false;
        }

    }


}
