﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public delegate void hpUpgrade();
    public hpUpgrade actualizarHP;
    public GameObject trunksController;
    public GameObject buuController;
    public Sprite[] vidaTrunksAux;
    public Sprite[] vidaBuuAux;
    public GameObject vidaTrunks;
    public GameObject vidaBuu;




    // Start is called before the first frame update
    void Start()
    {
        actualizarHP += actualizarHPGeneral;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //manager de vidas de ambos personajes funcional x delegados 
    void actualizarHPGeneral()
    {
        actualizarHPTrunks();
        actualizarHPBuu();
    }

    private void actualizarHPBuu()
    {
        if (buuController.GetComponent<BuuController>().HP == 100)
        {
            vidaBuu.GetComponent<SpriteRenderer>().sprite = vidaBuuAux[1];
        }
        if (buuController.GetComponent<BuuController>().HP == 80)
        {
            vidaBuu.GetComponent<SpriteRenderer>().sprite = vidaBuuAux[2];

        }
        if (buuController.GetComponent<BuuController>().HP == 60)
        {
            vidaBuu.GetComponent<SpriteRenderer>().sprite = vidaBuuAux[3];
        }
        if (buuController.GetComponent<BuuController>().HP == 40)
        {
            vidaBuu.GetComponent<SpriteRenderer>().sprite = vidaBuuAux[4];
        }
        if (buuController.GetComponent<BuuController>().HP == 20)
        {
            vidaBuu.GetComponent<SpriteRenderer>().sprite = vidaBuuAux[6];
        }
        if (buuController.GetComponent<BuuController>().HP == 0)
        {
            vidaBuu.GetComponent<SpriteRenderer>().sprite = vidaBuuAux[7];
        }
    }

    private void actualizarHPTrunks()
    {
        if (trunksController.GetComponent<TrunksController>().HP == 100)
        {
            vidaTrunks.GetComponent<SpriteRenderer>().sprite = vidaTrunksAux[1];
        }
        if (trunksController.GetComponent<TrunksController>().HP == 80)
        {
            vidaTrunks.GetComponent<SpriteRenderer>().sprite = vidaTrunksAux[2];

        }
        if (trunksController.GetComponent<TrunksController>().HP == 60)
        {
            vidaTrunks.GetComponent<SpriteRenderer>().sprite = vidaTrunksAux[3];
        }
        if (trunksController.GetComponent<TrunksController>().HP == 40)
        {
            vidaTrunks.GetComponent<SpriteRenderer>().sprite = vidaTrunksAux[4];
        }
        if (trunksController.GetComponent<TrunksController>().HP == 20)
        {
            vidaTrunks.GetComponent<SpriteRenderer>().sprite = vidaTrunksAux[6];
        }
        if (trunksController.GetComponent<TrunksController>().HP == 0)
        {
            vidaTrunks.GetComponent<SpriteRenderer>().sprite = vidaTrunksAux[7];
        }
    }
}
