﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proyectilManager : MonoBehaviour
{
    public float bounciness = 0.8f;
    bool rebotando = false;
    public GameObject particles;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Particles", 2f);
        this.GetComponentInChildren<CircleCollider2D>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(this.transform.position.y < -10)
        {
            Destroy(this.gameObject);
        }

        //Destroy(this.gameObject, 3f);
               
    }

    //manager de particulas, instanciacion y delete
    private void Particles()
    {
        //create
        GameObject myParticles;
        this.GetComponent<SpriteRenderer>().enabled = false;
        this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        this.GetComponent<Collider2D>().enabled = false;
        this.GetComponentInChildren<CircleCollider2D>().enabled = true;
        myParticles = Instantiate(particles, this.transform);
        myParticles.GetComponent<ParticleSystem>().Play();

        //delete
        Destroy(particles.gameObject, 1.5f);
        Destroy(this.gameObject, 1.5f);
    }

    private void FixedUpdate()
    {
        if (rebotando)
        {
            this.GetComponent<PolygonCollider2D>().sharedMaterial.bounciness = bounciness;
            rebotando = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        rebotando = true;

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        rebotando = true;
    }

}
