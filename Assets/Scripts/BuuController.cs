﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuuController : MonoBehaviour
{
    //variables
    public int HP = 100;
    public Vector2 jumpForce = new Vector2(0f,1f);
    public float movementF = 0.02f;
    public GameObject proyectil;
    public GameObject trunksController;
    public GameObject gameController;
    public float yForceHitted = 0.2f;
    public float xForceHitted = 0.5f;
    private bool isGrounded = true;
    private Animator anim;
    public bool bloqueo;
    private bool lookingLeft = true;
    private int comboStatus = 0;
    private int myHP = 100;
    float cooldownProyectil = 5f;
    Boolean cooldownProyectilB = false;
    Collider2D auxOther;
    public bool hit = true;

    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        #region Movement
        //MOVEMENT
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.position = new Vector2(this.transform.position.x - movementF, this.transform.position.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.transform.position = new Vector2(this.transform.position.x + movementF, this.transform.position.y);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (!lookingLeft)
            {
                this.transform.Rotate(new Vector3(0, 180, 0));
                lookingLeft = true;
            }
            anim.SetBool("finishRunning_Buu", false);
            anim.SetTrigger("Run_Buu");
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            if (lookingLeft)
            {
                this.transform.Rotate(new Vector3(0, 180, 0));
                lookingLeft = false;
            }
            anim.SetBool("finishRunning_Buu", false);
            anim.SetTrigger("Run_Buu");
        }
        else if (!Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A)) {
            anim.SetBool("finishRunning_Buu", true);
        }
        #endregion

        #region Shield
        //Shield
        if (Input.GetKey("g"))
        {
            bloqueo = true;
        }
        else
        {
            bloqueo = false;
        }
        #endregion

        #region Jump
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        { 
            anim.SetTrigger("Jump_Buu");
            this.GetComponent<Rigidbody2D>().AddForce(jumpForce, ForceMode2D.Impulse);
        }
        #endregion

        #region Attacks
        if (Input.GetKey(KeyCode.E))
        {
            StartCoroutine(attackCombo("e"));
        }
        else if (Input.GetKey(KeyCode.F))
        {
            StartCoroutine(attackCombo("f"));
        }

        IEnumerator attackCombo(string s)
        {
            if(s == "e")
            {
                if(comboStatus == 0)
                {
                    anim.SetTrigger("Attack1_Buu");
                    comboStatus = 1;
                    yield return new WaitForSeconds(0.5f);
                    if(comboStatus == 1)
                    {
                        print("combo break");
                        comboStatus = 0;
                    }
                }
            }
            if(s == "f")
            {
                if(comboStatus == 1)
                {
                    anim.SetTrigger("Attack2_Buu");
                    comboStatus = 0;
                }
            }

        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            lanzarProyectil();
        }

        void lanzarProyectil()
        {
            if (cooldownProyectilB == false) {
                GameObject aux;
                if (this.lookingLeft == true)
                {
                    aux = Instantiate(proyectil, new Vector3(this.GetComponent<Transform>().position.x - 1, this.GetComponent<Transform>().position.y + 1, 0), transform.rotation * Quaternion.Euler(0, 180, 0));
                    aux.GetComponent<Rigidbody2D>().AddForce(new Vector2(-100, 40));
                    cooldownProyectilB = true;
                    StartCoroutine("cdBomba");
                }
                else
                {
                    aux = Instantiate(proyectil, new Vector3(this.GetComponent<Transform>().position.x + 1, this.GetComponent<Transform>().position.y + 1, 0), Quaternion.identity);
                    aux.GetComponent<Rigidbody2D>().AddForce(new Vector2(100, 40));
                    cooldownProyectilB = true;
                    StartCoroutine("cdBomba");
                }
            }           
        }

        if (!auxOther && hit == false)
        {
            hit = true;
        }
        #endregion
    }

    //corroutine cooldown bomba 
    IEnumerator cdBomba()
    {
        yield return new WaitForSeconds(cooldownProyectil);
        cooldownProyectilB = false;
    }

    //fisicas al ser golpeado
    private void golpeadoAnim()
    {
        if (trunksController.transform.position.x > this.transform.position.x)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-xForceHitted, yForceHitted), ForceMode2D.Impulse);
        }
        else if (trunksController.transform.position.x < this.transform.position.x)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(xForceHitted, yForceHitted), ForceMode2D.Impulse);
        }

    }

    //controlador de vida funcional x delegados desde GameController
    private void actualizarVidaBuu()
    {
        HP -= 20;
        gameController.GetComponent<GameController>().actualizarHP();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "background")
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "background")
        {
            isGrounded = false;
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Hitbox_Trunks" && bloqueo == false && hit == true)
        {
            hit = false;
            golpeadoAnim();
            actualizarVidaBuu();
        }
        if (other.gameObject.tag == "Hitbox_Proyectil" && hit == true)
        {
            hit = false;
            actualizarVidaBuu();
            golpeadoAnim();
            auxOther = other;
        }

    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Hitbox_Trunks" && hit == false)
        {
            hit = true;
        }
    }





}

